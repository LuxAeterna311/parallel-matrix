package ru.akvine.matrix;

import java.util.Date;

public class Main {

    public static void main(String[] args) {

        final int ROWS = 500;
        final int COLUMNS = 700;

        Matrix firstMatrix = MatrixUtils.generateInt(1, 16, ROWS, COLUMNS);
        Matrix secondMatrix = MatrixUtils.generateInt(1, 16, COLUMNS, ROWS);

        long withoutMultithreading = calculateTimeWithoutMultithreading(firstMatrix, secondMatrix);
        long withMultithreading = calculateTimeWithMultithreading(firstMatrix, secondMatrix);

        System.out.println("[Without multithreading]: Time taken in milli seconds: " + withoutMultithreading);
        System.out.println("[Wit multithreading]: Time taken in milli seconds: " + withMultithreading);
    }

    public static long calculateTimeWithoutMultithreading(Matrix firstMatrix, Matrix secondMatrix) {
        Date start = new Date();
        firstMatrix.dot(secondMatrix);
        Date end = new Date();
        return end.getTime() - start.getTime();
    }

    public static long calculateTimeWithMultithreading(Matrix firstMatrix, Matrix secondMatrix) {
        Date start = new Date();
        MatrixUtils.multithreadingDot(firstMatrix, secondMatrix);
        Date end = new Date();
        return end.getTime() - start.getTime();
    }
}
