package ru.akvine.matrix;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class MatrixUtils {
    public static Matrix multithreadingDot(double[][] firstMatrix, double[][] secondMatrix) {
        double[][] result = new double[firstMatrix.length][secondMatrix[0].length];
        List<Thread> threads = new ArrayList<>();
        int rowCounts = firstMatrix.length;
        for (int i = 0; i < rowCounts; i++) {
            MatrixWorker task = new MatrixWorker(result, firstMatrix, secondMatrix, i);
            Thread thread = new Thread(task);
            thread.start();
            threads.add(thread);
            if (threads.size() % 10 == 0) {
                waitForThreads(threads);
            }
        }

        return new Matrix(result);
    }

    public static Matrix multithreadingDot(Matrix firstMatrix, Matrix secondMatrix) {
        return multithreadingDot(firstMatrix.getMatrix(), secondMatrix.getMatrix());
    }

    public static Matrix generateDouble(double min, double max, int rowsCount, int columnsCount) {
        double[][] values = new double[rowsCount][columnsCount];

        for (int i = 0; i < rowsCount; ++i) {
            for (int j = 0; j < columnsCount; ++j) {
                values[i][j] = ThreadLocalRandom.current().nextDouble(min, max);
            }
        }

        return new Matrix(values);
    }

    public static Matrix generateInt(int min, int max, int rowsCount, int columnsCount) {
        double[][] values = new double[rowsCount][columnsCount];

        for (int i = 0; i < rowsCount; ++i) {
            for (int j = 0; j < columnsCount; ++j) {
                values[i][j] = ThreadLocalRandom.current().nextInt(min, max);
            }
        }

        return new Matrix(values);
    }

    private static void waitForThreads(List<Thread> threads) {
        for (Thread thread : threads) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        threads.clear();
    }
}
