package ru.akvine.matrix;

public final class Matrix {
    private final int rowsCount;
    private final int columnsCount;
    private final double[][] matrix;

    public Matrix(int rowsCount, int columnsCount) {
        validate(rowsCount, columnsCount);
        this.rowsCount = rowsCount;
        this.columnsCount = columnsCount;
        this.matrix = new double[rowsCount][columnsCount];
    }

    public Matrix(int rowsCount, int columnsCount, double fillValue) {
        validate(rowsCount, columnsCount);
        this.rowsCount = rowsCount;
        this.columnsCount = columnsCount;
        this.matrix = new double[rowsCount][columnsCount];

        for (int i = 0; i < rowsCount; ++i) {
            for (int j = 0; j < columnsCount; ++j) {
                this.matrix[i][j] = fillValue;
            }
        }
    }

    public Matrix(double[][] values) {
        this.rowsCount = values.length;
        this.columnsCount = values[0].length;
        this.matrix = new double[rowsCount][columnsCount];

        for (int i = 0; i < rowsCount; ++i) {
            for (int j = 0; j < columnsCount; ++j) {
                this.matrix[i][j] = values[i][j];
            }
        }
    }

    public Matrix(double[] values) {
        this(values, false);
    }

    public Matrix(double[] values, boolean horizontalFilling) {
        this.rowsCount = values.length;
        this.columnsCount = values.length;
        this.matrix = new double[rowsCount][columnsCount];

        if (horizontalFilling) {
            for (int i = 0; i < rowsCount; ++i) {
                for (int j = 0; j < rowsCount; ++j) {
                    this.matrix[i][j] = values[i];
                }
            }
        } else {
            for (int i = 0; i < rowsCount; ++i) {
                for (int j = 0; j < rowsCount; ++j) {
                    this.matrix[j][i] = values[i];
                }
            }
        }
    }

    public Matrix negative() {
        double[][] matrix = new double[this.rowsCount][this.columnsCount];
        for (int i = 0; i < this.rowsCount; ++i) {
            for (int j = 0; j < this.columnsCount; ++j) {
                matrix[i][j] = -1 * this.matrix[i][j];
            }
        }

        return new Matrix(matrix);
    }

    public Matrix abs() {
        double[][] matrix = new double[this.rowsCount][this.columnsCount];
        for (int i = 0; i < this.rowsCount; ++i) {
            for (int j = 0; j < this.columnsCount; ++j) {
                matrix[i][j] = Math.abs(this.matrix[i][j]);
            }
        }

        return new Matrix(matrix);
    }

    public Matrix transpose() {
        double[][] matrix = new double[this.columnsCount][this.rowsCount];

        for (int i = 0; i < this.columnsCount; ++i) {
            for (int j = 0; j < this.rowsCount; ++j) {
                matrix[i][j] = this.matrix[j][i];
            }
        }

        return new Matrix(matrix);
    }

    public Matrix minus(double value) {
        return plus(new Matrix(this.rowsCount, this.columnsCount, -value));
    }

    public Matrix minus(double[][] values) {
        return minus(new Matrix(values));
    }

    public Matrix minus(Matrix values) {
        int rowsCount = values.getRowsCount();
        int columnsCount = values.getColumnsCount();

        for (int i = 0; i < rowsCount; ++i) {
            for (int j = 0; j < columnsCount; ++j) {
                values.getMatrix()[i][j] *= -1;
            }
        }

        return plus(values);
    }

    public Matrix plus(double value) {
        return plus(new Matrix(this.rowsCount, this.columnsCount, value));
    }

    public Matrix plus(double[][] values) {
        return plus(new Matrix(values));
    }

    public Matrix plus(Matrix values) {
        int rowsCount = values.getRowsCount();
        int columnsCount = values.getColumnsCount();

        for (int i = 0; i < rowsCount; ++i) {
            for (int j = 0; j < columnsCount; ++j) {
                values.getMatrix()[i][j] += this.matrix[i][j];
            }
        }

        return new Matrix(values.getMatrix());
    }

    public Matrix divide(double value) {
        return multiply(1 / value);
    }

    public Matrix divide(double[][] values) {
        return multiply(new Matrix(values));
    }

    public Matrix divide(Matrix values) {
        int rowsCount = values.getRowsCount();
        int columnsCount = values.getColumnsCount();

        for (int i = 0; i < rowsCount; ++i) {
            for (int j = 0; j < columnsCount; ++j) {
                values.getMatrix()[i][j] = 1 / values.getMatrix()[i][j];
            }
        }

        return multiply(values);
    }

    public Matrix multiply(double value) {
        double[][] matrix = new double[this.rowsCount][this.columnsCount];
        for (int i = 0; i < this.rowsCount; ++i) {
            for (int j = 0; j < this.rowsCount; ++j) {
                matrix[i][j] = this.matrix[i][j] * value;
            }
        }

        return new Matrix(matrix);
    }

    public Matrix multiply(double[][] values) {
        return multiply(new Matrix(values));
    }

    public Matrix multiply(Matrix values) {
        int rowsCount = values.getRowsCount();
        int columnsCount = values.getColumnsCount();

        for (int i = 0; i < rowsCount; ++i) {
            for (int j = 0; j < columnsCount; ++j) {
                values.getMatrix()[i][j] *= this.matrix[i][j];
            }
        }

        return new Matrix(values.getMatrix());
    }

    public Matrix pow(double value) {
        double[][] matrix = new double[this.rowsCount][this.columnsCount];
        for (int i = 0; i < this.rowsCount; ++i) {
            for (int j = 0; j < this.columnsCount; ++j) {
                matrix[i][j] = Math.pow(this.matrix[i][j], value);
            }
        }

        return new Matrix(matrix);
    }

    public Matrix pow(double[][] values) {
        return pow(new Matrix(values));
    }

    public Matrix pow(Matrix values) {
        int rowsCount = values.getRowsCount();
        int columnsCount = values.getColumnsCount();

        for (int i = 0; i < rowsCount; ++i) {
            for (int j = 0; j < columnsCount; ++j) {
                values.getMatrix()[i][j] = Math.pow(this.matrix[i][j], values.getMatrix()[i][j]);
            }
        }

        return new Matrix(values.getMatrix());
    }

    public Matrix dot(double[][] values) {
        return dot(new Matrix(values));
    }

    public Matrix dot(Matrix matrix) {
        validateDot(this.rowsCount, this.columnsCount, matrix.getRowsCount(), matrix.getColumnsCount());
        double[][] secondMatrix = matrix.getMatrix();
        int rowsCount = this.rowsCount;
        int columnsCount = secondMatrix[0].length;
        int secondMatrixRowsCount = secondMatrix.length;
        double[][] values = new double[rowsCount][columnsCount];

        for (int i = 0; i < rowsCount; ++i) {
            for (int j = 0; j < columnsCount; ++j) {
                for (int k = 0; k < secondMatrixRowsCount; ++k) {
                    values[i][j] += this.matrix[i][k] * secondMatrix[k][j];
                }
            }
        }

        return new Matrix(values);
    }

    @Override
    public String toString() {
        StringBuilder matrixOutput = new StringBuilder();
        for (int i = 0; i < this.rowsCount; i++) {
            for (int j = 0; j < this.columnsCount; j++) {
                matrixOutput.append(this.matrix[i][j]).append(" ");
            }
            matrixOutput.append("\n");
        }

        return matrixOutput.toString();
    }

    public int getRowsCount() {
        return rowsCount;
    }

    public int getColumnsCount() {
        return columnsCount;
    }

    public double[][] getMatrix() {
        return matrix;
    }

    private void validate(int rowsCount, int columnsCount) {
        if (rowsCount < 1 || columnsCount < 1) {
            throw new RuntimeException("Rows or columns count can't be less than 1");
        }
    }

    private void validateDot(int rowsCount, int columnsCount, int rowsCountSecond, int columnsCountSecond) {
        if (rowsCount != columnsCountSecond || columnsCount != rowsCountSecond) {
            throw new RuntimeException("Mismatch of matrix dimensions");
        }
    }
}
