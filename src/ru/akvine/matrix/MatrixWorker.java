package ru.akvine.matrix;

public class MatrixWorker implements Runnable {
    private final double[][] result;
    private final double[][] firstMatrix;
    private final double[][] secondMatrix;
    private final int rowNumber;

    public MatrixWorker(double[][] result, double[][] firstMatrix, double[][] secondMatrix, int rowNumber) {
        this.result = result;
        this.firstMatrix = firstMatrix;
        this.secondMatrix = secondMatrix;
        this.rowNumber = rowNumber;
    }

    @Override
    public void run() {
        for (int i = 0; i < secondMatrix[0].length; i++) {
            result[rowNumber][i] = 0;
            for (int j = 0; j < firstMatrix[rowNumber].length; j++) {
                result[rowNumber][i] += firstMatrix[rowNumber][j] * secondMatrix[j][i];
            }
        }
    }
}
